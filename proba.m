%Program in MatLab

%Enter the address of the data file into the auxiliary variables
pom_1 = 'C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-08.csv'
pom_2 = 'C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-09.csv'
pom_3 = 'C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-10.csv'
pom_4 = 'C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-11.csv'
pom_5 = 'C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-12.csv'
pom_6 = 'C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-13.csv'
pom_7 = 'C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-14.csv'

%Data recovery and a string format accessible to MatLab
fild_1 = fopen(pom_1)
C_1 = textscan(fild_1, '%q %q %q %q %q %q ','Delimiter', ';')

fild_2 = fopen(pom_2)
C_2 = textscan(fild_2, '%q %q %q %q %q %q ','Delimiter', ';')

fild_3 = fopen(pom_3)
C_3 = textscan(fild_3, '%q %q %q %q %q %q ','Delimiter', ';')

fild_4 = fopen(pom_4)
C_4 = textscan(fild_4, '%q %q %q %q %q %q ','Delimiter', ';')

fild_5 = fopen(pom_5)
C_5 = textscan(fild_5, '%q %q %q %q %q %q ','Delimiter', ';')

fild_6 = fopen(pom_6)
C_6 = textscan(fild_6, '%q %q %q %q %q %q ','Delimiter', ';')

fild_7 = fopen(pom_7)
C_7 = textscan(fild_7, '%q %q %q %q %q %q ','Delimiter', ';')

%The loop creates a matrix with the necessary data
for j=2:25
A(j-1,1)=datenum(strcat('2019-10-08',{' '},cell2mat(C_1{1}(j)))); %Date and time
A(j-1,2)=str2num(cell2mat(C_1{6}(j))); %Numeric parameter
A(j-1+24,1)=datenum(strcat('2019-10-09',{' '},cell2mat(C_2{1}(j))));
A(j-1+24,2)=str2num(cell2mat(C_2{6}(j)));
A(j-1+48,1)=datenum(strcat('2019-10-10',{' '},cell2mat(C_3{1}(j))));
A(j-1+48,2)=str2num(cell2mat(C_3{6}(j)));
A(j-1+72,1)=datenum(strcat('2019-10-11',{' '},cell2mat(C_4{1}(j))));
A(j-1+72,2)=str2num(cell2mat(C_4{6}(j)));
A(j-1+96,1)=datenum(strcat('2019-10-12',{' '},cell2mat(C_5{1}(j))));
A(j-1+96,2)=str2num(cell2mat(C_5{6}(j)));
A(j-1+120,1)=datenum(strcat('2019-10-13',{' '},cell2mat(C_6{1}(j))));
A(j-1+120,2)=str2num(cell2mat(C_6{6}(j)));
A(j-1+144,1)=datenum(strcat('2019-10-14',{' '},cell2mat(C_7{1}(j))));
A(j-1+144,2)=str2num(cell2mat(C_7{6}(j)));

end
B = A(A(1,:) == max(A(1,:)),:)
t1 = B(1,1)
data2 = datestr(t1),
disp('najwieksza wartosc st�enia wynosi:'), A(1,2)



