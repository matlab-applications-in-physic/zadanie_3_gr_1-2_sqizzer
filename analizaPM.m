%Data recovery and a string format accessible to MatLab
A_1 = fopen('C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-08.csv') %It opens the file
%These are the adresses where the files are located. Make sure that you have files in the same place
A_2 = textscan(A_1, '%q %q %q %q %q %q ','Delimiter', ';')   %deviding the files that they are written in a proper form. 

%It is done to all files 
B_1 = fopen('C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-09.csv')
B_2 = textscan(B_1, '%q %q %q %q %q %q ','Delimiter', ';')

C_1 = fopen('C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-10.csv')
C_2 = textscan(C_1, '%q %q %q %q %q %q ','Delimiter', ';')

D_1 = fopen('C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-11.csv')
D_2 = textscan(D_1, '%q %q %q %q %q %q ','Delimiter', ';')

E_1 = fopen('C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-12.csv')
E_2 = textscan(E_1, '%q %q %q %q %q %q ','Delimiter', ';')

F_1 = fopen('C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-13.csv')
F_2 = textscan(F_1, '%q %q %q %q %q %q ','Delimiter', ';')

G_1 = fopen('C:\Users\student\Desktop\matlabG\data\dane-pomiarowe_2019-10-14.csv')
G_2 = textscan(G_1, '%q %q %q %q %q %q ','Delimiter', ';')

%The loop creates a matrix with the necessary data. 
for j=2:25  %The loop goes from 2 to 25 although the file has more columns. I am doing it due to the fact that I only need those values which shows the time
A(j-1,1)=datenum(strcat('2019-10-08',{' '},cell2mat(A_2{1}(j))));  %converting date into a number and time from cell to matrix
A(j-1,2)=str2num(cell2mat(C_1{6}(j))); %Numeric parameter
A(j-1+24,1)=datenum(strcat('2019-10-09',{' '},cell2mat(B_2{1}(j))));
A(j-1+24,2)=str2num(cell2mat(C_2{6}(j)));
A(j-1+48,1)=datenum(strcat('2019-10-10',{' '},cell2mat(C_2{1}(j))));
A(j-1+48,2)=str2num(cell2mat(C_3{6}(j)));
A(j-1+72,1)=datenum(strcat('2019-10-11',{' '},cell2mat(D_2{1}(j))));
A(j-1+72,2)=str2num(cell2mat(C_4{6}(j)));
A(j-1+96,1)=datenum(strcat('2019-10-12',{' '},cell2mat(E_2{1}(j))));
A(j-1+96,2)=str2num(cell2mat(C_5{6}(j)));
A(j-1+120,1)=datenum(strcat('2019-10-13',{' '},cell2mat(F_2{1}(j))));
A(j-1+120,2)=str2num(cell2mat(C_6{6}(j)));
A(j-1+144,1)=datenum(strcat('2019-10-14',{' '},cell2mat(G_2{1}(j))));
A(j-1+144,2)=str2num(cell2mat(C_7{6}(j)));

end

B = A(A(1,:) == max(A(1,:)),:) %searching for the greatest value
t1 = B(1,1)  
data2 = datestr(t1),     %converting number to date
disp('najwieksza wartosc stezenia wynosi:'), A(1,2) %displaying the greatest PM10 level and its date

